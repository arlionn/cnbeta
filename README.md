# cnbeta

## 修改建议
- `2019/1/13 20:36`
  - 可以增加几个完整的Stata 范例，介绍 `cnbeta` 的具体应用；
  - 在帮助文件中介绍 `regfit` 命令的下载方法：(1) 使用 `ssc install github` 安装 `github` 命令；(2) 使用 `github install arlionn/regfit` 命令从 Github 上安装 `regfit` 命令。
  - 把你已经做好的 `cnbeta` 相关命令放到 Github 上，参见 [`regfit` 主页](https://github.com/arlionn/regfit)。Note: 你需要额外建立两个文件：**cnbeta.pkg** 和 **stata.toc**，这两个文件是协助检索的。

## NEWs
- `2019/1/12 8:43` 胡杰，请把已经完成的代码传上来。如果有更新，可以直接覆盖旧版本的文件，码云有版本控制能力，回头可以回看以前的版本，并在不同版本之间比对。

## 介绍
胡杰 cnbeta.ado 项目



## Stata 范例
TBD
